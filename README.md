# AML

## Alternative markup language framework

	Check syntax with yaml to xml tool:
	[https://codebeautify.org/yaml-to-json-xml-csv]([https://codebeautify.org/yaml-to-json-xml-csv])

```
TODO:
* Support for array substitution in variabliser
* Code improvement
```
___

### index.js
	* this is a core script for the AML engine
	* variabliser function allows injecting variables into markup roughly following Bash parameter substitution
	* `window.variables` parameter provides a list of variables
 	* Current version only supports basic substitution.

 [Bash parameter substitution] (https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html)


# Firefox

Local debugging requires (about:config) flag:

`security.fileuri.strict_origin_policy = false`

___

## Foreign dependencies:

### json2xml.min.js
	This work is licensed under Creative Commons GNU LGPL License.
	License:  http://creativecommons.org/licenses/LGPL/2.1/
	Version:  0.9m
	Author:   Stefan Goessner/2006
	Modified: Mikhail Timofeev/2019
	Web:      https://goessner.net/download/prj/jsonxml/json2xml.js



## yaml.min.js
	Copyright (c) 2010, Yahoo! Inc. All rights reserved.
	License:  http://developer.yahoo.com/yui/license.html
	version:  3.2.0
	build:    2676
	Modified: Mikhail Timofeev/2019

