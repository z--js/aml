"use strict";

/*
import { json2xml } from '../extractors/json2xml.min.js';
import { YAML } from '../extractors/yaml.min.js';
*/

const renameKey = (
	obj,
	oldKey,
	newKey
) => {
	if (
		obj [oldKey] == undefined
	){
		return false;
	}
	else {
		obj [newKey] = obj [oldKey];
		delete obj [oldKey];
		return true;
	}
};

/*
	const reset = () => {
		let removables = localStorage.length;
		localStorage.clear ();
		return removables;
	};
*/

const addToCache = (
	fileName,
	value,
	extraArgs
) => {
	if (
		value == undefined
		|| value.trim ().length < 2
	) {
		renameKey (
			window.resources [extraArgs.mimeType],
			'?' + extraArgs.assetId,
			'!' + extraArgs.assetId
		);
		//console.log ('Unable to cache file:', fileName);
		return false;
	}
	else {
		localStorage.setItem (
			extraArgs.assetId,
			value
		);
		renameKey (
			window.resources [extraArgs.mimeType],
			'?' + extraArgs.assetId,
			'+' + extraArgs.assetId
		);
		//console.log ('File added to cache:', fileName);
		return true;
	}
};

const extract = (
	mimeType, // mime-type of file
	string // file sources
) => {
	if (
		typeof MIMETYPES !== 'undefined'
		//&& typeof MIMETYPES[mimeType] === 'function'
	) {
		return MIMETYPES[mimeType](string);
	}
	return string;
};

const readFile = (
	fileName, // path to file being loaded
	mimeType, // mime-type of file
	callback, // function to execute on completion
	extraArgs // optional arguments for callback
) => {
	var rawFile = new XMLHttpRequest ();
	rawFile.overrideMimeType (
		mimeType
	);
	rawFile.open (
		'GET',
		fileName,
		true
	);
	rawFile.onreadystatechange = () => {
		if (
			rawFile.readyState === 4
		) {
			if (
				rawFile.status === 200
				|| rawFile.status == 0
			) {
				callback (
					fileName,
					extract (
						mimeType,
						rawFile.responseText
					),
					extraArgs
				);
			}
		}
	};
	rawFile.onerror = () => {
		callback (
			fileName,
			undefined,
			extraArgs
		);
	};
	rawFile.send (null);
};

const loadFile = (
	assetId,
	mimeType = 'text/plain'
) => {
	let item = window.resources [mimeType] [assetId];

	renameKey (
		window.resources [mimeType],
		'@' + assetId,
		'?' + assetId
	);

	let chunk = localStorage.getItem (assetId);
	if (
		!chunk
	) {
		//console.log('File',item,'is not cached yet');
		readFile (
			item,
			mimeType,
			addToCache,
			{
				mimeType: mimeType,
				assetId: assetId
			}
		);
	}
	else {
		//console.log('File', item,'is already cached');
		renameKey (
			window.resources [mimeType],
			'?' + assetId,
			'+' + assetId
		);
		return true;
	}
};

const loadAssets = (
	manifestFile, // one-time procedure to load manifest
	manifestText,
	extraArgs
) => {
	console.log ('Manifest loaded:', manifestFile);
	// console.log(manifestText);
	window.resources = JSON.parse (manifestText).resources;

	Object.keys(window.resources).forEach (
		function (mimeType) {
			if (
				mimeType [0] !== '-' // property names start with '-'
			) {
				Object.keys(window.resources [mimeType]).forEach (
					function (assetId) {
						if (
							assetId [0] !== '-' // properties start with '-'
						) {
							loadFile (
								assetId,
								mimeType
							);
						}
					}
				);
			}
		}
	);
	//console.log('All assets processed. Filling templates');

	// load aml modules, passing env variables
	readFile ('../variables', 'application/json', start, null);
};

const variablise = (
  string,
  callback
) => {
	for (
		var i = 0;
		i < localStorage.length;
		i += 1
	){
		string = string.replace (
      new RegExp (
        '\\${' + localStorage.key (i) + '}',
        'g'
      ),
      localStorage.getItem (localStorage.key (i))
    );
	}

  for (
    var envVariable in window.variables || []
  ) {
    string = string.replace (
      new RegExp (
        '\\${' + envVariable + '}',
        'g'
      ),
      window.variables [envVariable]
    );
	}

	let missingRes = string.match (
		new RegExp (
			'\\${[^}]*}',
			'g'
		)
	) || [];

	missingRes.forEach (
		(resource) => {
			let replacement = resource.slice (2, -1);
			console.log(replacement, 'resource is missing');
			string = string.replace (
				resource,
				'<!-- resource / ' + replacement + ' -->'
			);
		}
	);

	// let missingVar = string.match (
	// 	new RegExp (
	// 		'\\$@{[^}]*}',
	// 		'g'
	// 	)
	// ) || [];

	// missingVar.forEach (
	// 	(variable) => {
	// 		let replacement = variable.slice (3, -1);
	// 		console.log(replacement, 'variable is missing');
	// 		string = string.replace (
	// 			variable,
	// 			'<!-- variable / ' + replacement + ' -->'
	// 		);
	// 	}
	// );

	callback (string);
};

const start = (
	fileName,
	variables,
	extraArgs
) => {
	window.variables = JSON.parse (variables);
	let viewName = window.location.hash.substr (1) || 'index';

	// console.log(viewName)

	// getting content root
	let view = localStorage.getItem ('views.' + viewName) || localStorage.getItem ('views.404');

	/*
	let style = document.createElement('style');
	mainStyle.type = 'text/css';
	stylePack =
	mainStyle.text = stylePack.join('\n\n');
	document.body.appendChild(style);
	*/

	//let mainContent = .join('');
	//document.body.innerHTML = mainContent;

	/*
	let mainScript = document.createElement('script');
	mainScript.type = 'text/javascript';
	mainScript.text = scriptPack.join('\n\n');
	document.body.appendChild(mainScript);
	*/

	// other manifest operations
	variablise (view, display);
};

const display = (body) => {
	document.body.innerHTML = body;
	console.timeEnd ('Caching');
};

(() => {
	console.time ('Caching');

	window.resources = {};

	readFile (
		document.querySelectorAll ('[rel="manifest"]') [0].getAttribute ('href'),
		'application/manifest+json',
		loadAssets, // callback
		false
	);
}) ();

window.onhashchange = () => { location.reload (); };
