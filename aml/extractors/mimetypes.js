const MIMETYPES = {
  'application/x-yaml': (string) => {
    let conv = json2xml (
      JSON.parse(
        JSON.stringify (
          YAML.parse (string),
          null,
          0
        ),
        0
      )
    );
    return conv;
  },

  'image/svg+xml': (string) => {return string;},
  'application/json': (string) => {return string;},
  'application/manifest+json': (string) => {return string;},
  'text/css': (string) => {return string;},
  'text/javascript': (string) => {return string;}
}
